package com.levelup.tictactoe;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainApplicationJAVAFX extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        MainController controller = SpringFXMLLoader.load("/main.fxml");
        Scene scene = new Scene((Parent) controller.getView(), 600, 400);
        primaryStage.setTitle("App");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
