package com.levelup.tictactoe;

import com.levelup.tictactoe.service.Game;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


@Component
public class MainController {

    @Autowired
    Game game;

    @FXML
    Rectangle cell1;

    @FXML
    Rectangle cell2;

    @FXML
    Rectangle cell3;

    @FXML
    Rectangle cell4;

    @FXML
    Rectangle cell5;

    @FXML
    Rectangle cell6;

    @FXML
    Rectangle cell7;

    @FXML
    Rectangle cell8;

    @FXML
    Rectangle cell9;

    @FXML
    TextArea textlogArea;

    @FXML
    TextField ipTextField;

    @FXML
    TextField portTextField;

    @FXML
    CheckBox isServerCHeckbox;

    boolean ourTurn;

    Map<Rectangle, Integer> coords = new HashMap<>();

    public void initialize() {
        coords.put(cell1, 1);
        coords.put(cell2, 2);
        coords.put(cell3, 3);
        coords.put(cell4, 4);
        coords.put(cell5, 5);
        coords.put(cell6, 6);
        coords.put(cell7, 7);
        coords.put(cell8, 8);
        coords.put(cell9, 9);
    }

    @FXML
    void cellClicked(MouseEvent event) throws IOException {
        Rectangle source = (Rectangle) event.getSource();
        if (!ourTurn || source.getFill().equals(Color.DARKGREEN) || source.getFill().equals(Color.DARKRED)) {
            textlogArea.appendText("!!!CHEATER!!!" + "\n");
            return;
        }
        source.setFill(Color.DARKGREEN);
        textlogArea.appendText(coords.get(source) + "\n");
        Integer coord = coords.get(source);
        game.makeMove(coord);
        ourTurn = false;
        if (game.checkWin()) {
            textlogArea.appendText("Somebody won!");
        } else if (game.checkDraw()) {
            textlogArea.appendText("Draw!");
        } else {
            waitMove();
        }
    }

    @FXML
    void connect() throws IOException {
        boolean isServer = isServerCHeckbox.isSelected();
        String ip = ipTextField.getText();
        Integer port = Integer.parseInt(portTextField.getText());
        ourTurn = game.start(isServer, ip, port);
        if (!ourTurn) {
            waitMove();
        }
    }

    private void waitMove() {
        new Thread(() -> {
            try {
                int move = game.waitMove();
                Optional<Map.Entry<Rectangle, Integer>> first =
                        coords.entrySet().stream().filter(entry -> entry.getValue().equals(move)).findFirst();
                if (first.isPresent()) {
                    Rectangle cell = first.get().getKey();
                   Platform.runLater( () -> cell.setFill(Color.DARKRED));
                } else {
                    textlogArea.appendText("They are trying to hack us!");
                    waitMove();
                }
                ourTurn = true;
                if (game.checkWin()) {
                    textlogArea.appendText("Somebody won!");
                } else if (game.checkDraw()) {
                    textlogArea.appendText("Draw!");
                }
            } catch (IOException e) {
                e.printStackTrace();
                waitMove();
            }
        }).start();
    }

    private Node view;


    public Node getView() {
        return view;
    }

    public void setView(Node view) {
        this.view = view;
    }
}
